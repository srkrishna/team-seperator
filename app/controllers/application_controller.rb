require "#{Rails.root}/lib/import_xcel.rb"
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  require 'csv'
  include ImportXcel
  before_action :set_file,:set_file_data, only: [:import]

  def index
  end

  def import
      header = @data.row(1)
      no_rows = @data.last_row
      if header.present?
        # byebug
        if (no_rows -1 ) < params[:team_count].to_i
          flash[:notice] = "Team count is greater then members"
          redirect_to root_path
        else
          csv_data = get_result(no_rows, params[:team_count],@data, header)
          send_data csv_data, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment;filename=testing.xls"

      end
    end
  end

  private
  def set_file
    @file = params[:file]
    if @file.present?
      ext = ["application/vnd.oasis.opendocument.spreadsheet", "text/csv", "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
      unless ext.include?(@file.content_type)
        flash[:notice] = "Unknown file format!"
        redirect_to root_path
      end
    else
      flash[:notice] = "Add file before form submit"
      redirect_to root_path
    end
  end

  def set_file_data
    # @file = params[:file]
    @data = Roo::Spreadsheet.open(@file.path)
    if @data.last_row <= 1
      flash[:notice] = "Insufficient data!"
      redirect_to root_path
    end
  end

end
