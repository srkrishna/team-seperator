module ImportXcel

  def get_result(no_rows, team_member_count,data, header)
    @data = data
    if (no_rows -1 ) % params[:team_count].to_i == 0
      @msg = "Team count is #{no_rows/params[:team_count].to_i} with members of #{params[:team_count].to_i }"
      @team_count = no_rows/params[:team_count].to_i
      remainder = no_rows/params[:team_count].to_i
    else
      @msg = " #{no_rows.gcd(params[:team_count].to_i)} members has no team "
      @team_count = no_rows/params[:team_count].to_i
      remainder = no_rows.gcd(params[:team_count].to_i)
    end

    @record = []
    (2..@data.last_row).each do |row_id|
      headers = Hash.new
      row_data = @data.row(row_id)
      header.each_with_index {|row_header,i| headers[row_header.downcase] = row_data[i] }
      @record << headers
    end

    @tmp = @record
    @final = []
    while @final.count < @team_count
      @team = []
      @tmp.shuffle.each do |test|
        if @team.count < @team_count and check_team(@team,test["college"]) and @team.count < params[:team_count].to_i
          @team << test
          @tmp.delete_if { |h| h["email"] == test["email"] }
        end
      end
      # binding.pry
      @final << @team if @team.present?
    end

    @final.each do |final|
      @tmp.each do |tmp|
        if final.count < params[:team_count].to_i and check_team(final,tmp["college"])
          final << tmp
          @tmp.delete_if { |h| h["email"] == tmp["email"] }
        end
      end
    end

    if @tmp.present?
      @final << @tmp
    end

    team_id = 1
    csv_file = CSV.generate({}) do |csv|
      csv << (header << "Team")
      @final.each do |row|
        row.each do |inside_row|
          csv << (inside_row.values << team_id)
        end
        team_id += 1
      end
    end
    return csv_file
  end

  def check_team(team_data,key)
    if team_data.any?{|has| has["college"] == key }
      return false
    else
      return true
    end
  end

end
